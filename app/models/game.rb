class Game < ActiveRecord::Base
	acts_as_votable
	# has_attached_file :game_image, :styles => { :medium => "300x300>", :thumb => "100x100>" }
	# validates :game_image, :attachment_presence => true
	validates_presence_of  :description
	validates :title, presence: true, uniqueness: { case_sensitive: false }
end
