class User < ActiveRecord::Base
	# Include default devise modules. Others available are:
	# :token_authenticatable, :confirmable,
	# :lockable, :timeoutable and :omniauthable
	devise :database_authenticatable, :registerable,
    	:recoverable, :rememberable, :trackable, :validatable
	VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i
  	validates :email, format: { with: VALID_EMAIL_REGEX }
    acts_as_voter

    validates_presence_of :fullName, :country
end
