json.array!(@games) do |game|
  json.extract! game, :title, :description, :video_url, :facebook_url, :twitter_url, :web_url
  json.url game_url(game, format: :json)
end