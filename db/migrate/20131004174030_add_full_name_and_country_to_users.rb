class AddFullNameAndCountryToUsers < ActiveRecord::Migration
  def change
    add_column :users, :fullName, :string
    add_column :users, :country, :string
  end
end
