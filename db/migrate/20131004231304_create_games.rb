class CreateGames < ActiveRecord::Migration
  def change
    create_table :games do |t|
      t.string :title
      t.string :description
      t.string :video_url
      t.string :facebook_url
      t.string :twitter_url
      t.string :web_url

      t.timestamps
    end
  end
end
