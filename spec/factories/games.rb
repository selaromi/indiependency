require 'faker'

FactoryGirl.define do
  factory :game do
    title { Faker::Name.title }
    description { Faker::Lorem.paragraph }
    video_url { Faker::Internet.url }
    facebook_url { Faker::Internet.url }
    twitter_url { Faker::Internet.url }
    # required if the Devise Confirmable module is used
    # confirmed_at Time.now
  end
end