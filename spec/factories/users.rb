require 'faker'

FactoryGirl.define do
  factory :user do
    fullName { Faker::Name.name }
    country { Faker::Address.country }
    email { Faker::Internet.email } 
    password { Faker::Internet.password }
    password_confirmation { password }
    # required if the Devise Confirmable module is used
    # confirmed_at Time.now
  end
end