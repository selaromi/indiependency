require 'spec_helper'
require 'pp'

describe Game do 

	it "has a valid factory" do
		expect(build(:game)).to be_valid
	end

	it "is invalid when title is not present" do
		game = build(:game,title: nil)
		expect(game).not_to be_valid
	end

	it "is invalid when description is not present" do
		game = build(:game,description: nil)
		expect(game).not_to be_valid
	end

	it "is invalid when title is already taken" do
		create(:game, title:"myTitle")
		user = build(:game, title: "MYTITLE")
		expect(user).to have(1).errors_on(:title)
	end
	
end