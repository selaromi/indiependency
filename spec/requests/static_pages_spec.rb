require 'spec_helper'

	describe "Static Pages" do
		describe "Home Page" do
			it "Should have the content 'Thank You'" do
	      # Run the generator again with the --webrat flag if you want to use webrat methods/matchers
	      visit root_path
	      expect(page).to have_content('Thank You!')
	  end
	end

	describe "Help page" do

		it "should have the content 'Help'" do
			visit help_path
			expect(page).to have_content('Help')
		end
	end

	describe "About page" do

		it "should have the content 'About Us'" do
			visit '/static_pages/about'
			expect(page).to have_content('About Us')
		end
	end
end
